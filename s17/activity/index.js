

/*
	1. Create a function named printUserInfo() which is able to display a user's to fullname, age, location and other information.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
*/
function printUserInfo(){
	console.log("Hello, I'm John Doe.");
	console.log("I am 25 years old.");
	console.log("I live in 123 street, Quezon City");
	console.log("I have a cat named Joe.");
	console.log("I have a dog named Danny.");
}
printUserInfo();

/*
	2. Create a function named printFiveBands which is able to display 5 bands/musical artists.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/

function printFiveBands(){
	console.log("The Beatles");
	console.log("Taylor Swift");
	console.log("The Eagles");
	console.log("Rivermaya");
	console.log("Eraserheads");
}
printFiveBands();

/*
	3. Create a function named printFiveMovies which is able to display the name of 5 movies.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
*/

function printFiveMovies(){
	console.log("Lion King");
	console.log("Howl's Moving Castle");
	console.log("Meet the Robinsons");
	console.log("School of Rock");
	console.log("Sprited Away");
}
printFiveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.

		-invoke the function to display information similar to the expected output in the console.
*/


function printFriends(){
	let friend1 = "Eugene"; 
	let friend2 = "Dennis"; 
	let friend3 = "Vincent";

	console.log("These are my friends:");
	console.log(friend1); // variables are not defined in global scope
	console.log(friend2); // variables are not defined in global scope
	console.log(friend3); 
	// console.log(true); not included to the expected output
	// console.log(friends); variable is not declared
};

printFriends(); // add parentheses and semi-colon "();"" to invoke the function



//Do not modify
//For exporting to test.js
try{
	module.exports = {
		printUserInfo,
		printFiveBands,
		printFiveMovies,
		printFriends
	}
} catch(err){

}